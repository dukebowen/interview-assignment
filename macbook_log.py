import json
import re
from datetime import datetime


INPUT = 'C:\\Users\\0086\\Documents\\Personal\\Interview\\Dev-Ops\\DevOps_interview_data_set\\DevOps_interview_data_set'
OUTPUT = 'C:\\Users\\0086\\Documents\\Personal\\Interview\\Dev-Ops\\DevOps_interview_data_set\\DevOps_interview_data_set.json'


def main():
    with open(INPUT, 'r') as rf:
        parsed_logs = []
        log_dict = {}
        log_text = rf.read()
        log_list = re.findall(
            r'^(\S+ \d+ \d{2}:\d{2}:\d{2}) '
            r'(?:--- last message repeated (\d+) times? ---|'
            r'(\S+) (.+?)\[(\d+)\](?: \((.+?)(?:\[(\d+)\])?\))?: (.+))', log_text, re.M)
        print(len(log_list))
        for index, log in enumerate(log_list):
            if log[1]:
                repeated_times = int(log[1])
                log_params = log_list[index-1][2:]
            else:
                repeated_times = 1
                log_params = log[2:]
            devname, pname, pid, subpname, subpid, descr = log_params
            time = datetime.strptime(log[0], '%b %d %H:%M:%S')
            log_store = log_dict.setdefault(f'{devname},{pname},{pid},{descr}', {})
            time_key = '%02d00-%02d00' % (time.hour, (time.hour + 1) % 24)
            if time_key in log_store:
                log_store[time_key] += repeated_times
            else:
                log_store[time_key] = repeated_times
        for log, time_win in log_dict.items():
            devname, pname, pid, descr = log.split(',', 3)
            parsed_logs.append({
                'deviceName': devname,
                'processId': pid,
                'processName': pname,
                'description': descr,
                'timeWindow': time_win,
            })
        with open(OUTPUT, 'w') as wf:
            wf.write(json.dumps(parsed_logs, indent=2))


if __name__ == "__main__":
    main()

#!/usr/bin/env python3
from aws_cdk import (
    aws_ec2 as ec2,
    aws_ecs as ecs,
    aws_ecs_patterns as ecs_patterns,
    aws_elasticloadbalancingv2 as elbv2,
    core,
)


class BonjourFargate(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, *kwargs, env=core.Environment(
            account='382348112289', region='cn-northwest-1'))

        # Create VPC and Fargate Cluster
        # NOTE: Limit AZs to avoid reaching resource quotas
        vpc = ec2.Vpc(
            self, 'ecs-test-Vpc',
            nat_gateways=0,
            max_azs=2,
            # subnet_configuration=[ec2.SubnetConfiguration(
            #     name='ecs-test-private',
            #     subnet_type=ec2.SubnetType.ISOLATED)]
        )

        security_group = ec2.SecurityGroup(
            self, 'ecs-test-SecurityGroup',
            allow_all_outbound=False,
            vpc=vpc,
        )

        security_group.add_ingress_rule(
            peer = ec2.Peer.ipv4(vpc.vpc_cidr_block),
            connection = ec2.Port.tcp(80),
            description='Allow http inbound from VPC'
        )

        cluster = ecs.Cluster(
            self, 'ecs-test-Ec2Cluster',
            vpc=vpc
        )

        # Create Task Definition
        task_definition = ecs.FargateTaskDefinition(
            self, 'TaskDef')
        container = task_definition.add_container(
            'web',
            image=ecs.ContainerImage.from_registry('amazon/amazon-ecs-sample'),
            memory_limit_mib=256
        )
        port_mapping = ecs.PortMapping(
            container_port=80,
            protocol=ecs.Protocol.TCP
        )
        container.add_port_mappings(port_mapping)

        fargate_service = ecs.FargateService(
            self, 'ecs-test-FargateService',
            cluster=cluster,
            task_definition=task_definition,
            security_groups=[security_group],
        )

        # Create ALB
        lb = elbv2.ApplicationLoadBalancer(
            self, 'LB',
            vpc=vpc,
            internet_facing=True
        )
        listener = lb.add_listener(
            'PublicListener',
            port=80,
            open=True
        )

        health_check = elbv2.HealthCheck(
            interval=core.Duration.seconds(60),
            path='/health',
            timeout=core.Duration.seconds(5)
        )

        # Attach ALB to ECS Service
        listener.add_targets(
            'ECS',
            port=80,
            targets=[fargate_service],
            health_check=health_check,
        )

        core.CfnOutput(
            self, 'ecs-test-LoadBalancerDNS',
            value=lb.load_balancer_dns_name
        )

app = core.App()
BonjourFargate(app, 'Duke-ECS-TEST')
app.synth()

#### app.py
#!/usr/bin/env python3

from aws_cdk import core

from hello_cdk.hello_cdk_stack import BonjourFargate


app = core.App()
BonjourFargate(app, "hello-cdk")

app.synth()

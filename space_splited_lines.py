input_path = 'C:\\Users\\0086\\Documents\\Personal\\Interview\\Dev-Ops\\sort.txt'
outpit_path = 'C:\\Users\\0086\\Documents\\Personal\\Interview\\Dev-Ops\\output.txt'

with open(input_path) as fr:
    lines = fr.readlines()
    line_strs = [line.split() for line in lines]
    line_strs.sort(key=lambda x: int(x[1]))
    line_strs = [' '.join(strs) for strs in line_strs]
    with open(outpit_path, 'w') as fw:
        fw.write('\n'.join(line_strs))

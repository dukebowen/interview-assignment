$log_file = "data_set"
$log_server_url = https://foo.com/bar"
$line_breaker = "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"

$errorCount = 0
$errorRecord = @{}

Function getHash($str) {
    Return $(Get-FileHash -InputStream ([System.IO.MemoryStream]::New([System.Text.Encoding]::UTF8.GetBytes($str)))).Hash
}

Function breakCheck($month_mark) {
    Return $line_breaker.contains($month_mark)
}

Function parseMessage($single_message) {
    if ($single_message.contains("error")) {
        $sets = $single_message.split(" ", 5)

        $timestamp = $sets[0] + " " + $sets[1] + " " + $sets[2]
        $hostname = $sets[3]
        $messageBody = $sets[4]

        $processInfo = $messageBody.split(":", 2)[0]
        $description = $messageBody.split(":", 2)[1]

        $processName = $processInfo.split("[")[0]
        $processId = $processInfo.split("[")[1].split("]")[0]

        $errorCount ++
        $recordId = getHash($hostname + $processName + $description)
        if (!($errorRecord.ContainsKey($recordId))) {
            $recordDetail = @{}
            $recordDetail["deviceName"] = $hostname
            $recordDetail["processId"] = $processId
            $recordDetail["processName"] = $processName
            $recordDetail["description"] = $description
            $recordDetail["timeWindow"] = $timestamp, $timestamp
            $recordDetail["numberOfOccurrence"] = 1
            $errorRecord[$recordId] = $recordDetail
        }
        else {
            $errorRecord[$recordId]["timeWindow"][1] = $timestamp
            $errorRecord[$recordId]["numberOfOccurrence"] = $errorRecord[$recordId]["numberOfOccurrence"] + 1
        }
    }
}

$single_record = ""
$line_num = 1

foreach($single_line in Get-Content $log_file) {
    $single_record = $single_line
    $line_num ++

    if (breakCheck($single_line.split(" ")[0]) and $line_num > 1) {
        $single_record = $single_record + $single_line
    }
    else {
        parseMessage($single_record)
        $single_record = ""
    }
}

Invoke-WebRequest $log_server_url -Method POST -Body ($errorRecord | ConvertTo-Json -Depth 4) -ContentType "application/json"
